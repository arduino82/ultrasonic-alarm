//www.elegoo.com
//2016.12.08
#include "SR04.h"

const int TRIGGER_PIN = 8;
const int ECHO_PIN = 6;
const int BUZZER_PIN = 12;
const int DEVIATION = 3;
const char MEASURMENT_UNIT[] = "cm";

const SR04 sr04 = SR04(ECHO_PIN, TRIGGER_PIN);
long lastDistance = 1180;  // Set according to initial distance
long currentDistance;

void activateAlarm(int activationDuration, int activationRepetitions) {
   for(int i=0; i < activationRepetitions; i++) {
    // Activate alarm
    digitalWrite(BUZZER_PIN, HIGH);
    digitalWrite(LED_BUILTIN, HIGH);    
    delay(activationDuration);

    // Deactivate alarm
    digitalWrite(BUZZER_PIN, LOW);
    digitalWrite(LED_BUILTIN, LOW);
    delay(activationDuration);
  }
}

void printCurrentDistance(long currentDistance, char measurementUnit[]) {
  Serial.print(currentDistance);
  Serial.println(measurementUnit);  
}

void setup() {
  Serial.begin(9600);
  pinMode(BUZZER_PIN, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {
  currentDistance = sr04.Distance();
  printCurrentDistance(currentDistance, MEASURMENT_UNIT);
    
  if (currentDistance < lastDistance - DEVIATION || currentDistance > lastDistance + DEVIATION) {
    activateAlarm(200, 3);
  }

  lastDistance = currentDistance;
}
