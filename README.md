# Ultrasonic Alarm



## Component List


| Component | Quantity |
| ------ | ------ |
| Arduino Uno R3 | 1 |
| 5mm LED | 1 |
| Ultrasonic Distance Sensor | 1 |
| Piezo active buzzer | 1 |


## Circuit Schematic
[Can be found here](https://gitlab.com/arduino82/ultrasonic-alarm/-/blob/09c260b37ff874719902363c201807bc72d32f8d/Circuit_Shematic.svg)


## Code
[Can be found here](https://gitlab.com/arduino82/ultrasonic-alarm/-/blob/09c260b37ff874719902363c201807bc72d32f8d/Ultrasonic_Alarm.ino)
